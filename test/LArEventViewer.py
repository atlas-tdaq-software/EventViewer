#!/usr/bin/env tdaq_python

from EventViewer import EventViewer
import LArEventViewerPlugin

if __name__ == '__main__':
    w = EventViewer.EventViewer()
    w.run()
