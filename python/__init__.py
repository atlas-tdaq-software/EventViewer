"""ATLAS events viewer
"""
__author__ = "Serguei Kolos (Serguei.Kolos@cern.ch)"
__version__ = "$Revision: 1.0 $"
__date__ = "$Date: 2013/06/05 21:57:20 $"
__all__ = ["EventViewer"]

from . import EventViewer
from .Plugin import Plugin
