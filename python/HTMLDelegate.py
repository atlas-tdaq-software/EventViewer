from PyQt5 import QtCore, QtGui, QtWidgets

class HTMLDelegate(QtWidgets.QStyledItemDelegate):
    
    def __init__(self, parent):
        super(HTMLDelegate, self).__init__(parent)
        
    def paint(self, painter, option, index):
        options = QtWidgets.QStyleOptionViewItem(option)
        self.initStyleOption(options,index)

        style = QtGui.QApplication.style() if options.widget is None else options.widget.style()

        doc = QtGui.QTextDocument()
        doc.setHtml(options.text)

        options.text = ""
        style.drawControl(QtWidgets.QStyle.CE_ItemViewItem, options, painter);

        ctx = QtGui.QAbstractTextDocumentLayout.PaintContext()

        # Highlighting text if item is selected
        if options.state & QtWidgets.QStyle.State_Selected:
            ctx.palette.setColor(QtGui.QPalette.Text, options.palette.color(
                    QtGui.QPalette.Active, QtGui.QPalette.HighlightedText));

        textRect = style.subElementRect(QtWidgets.QStyle.SE_ItemViewItemText, options)
        painter.save()
        painter.translate(textRect.topLeft())
        painter.setClipRect(textRect.translated(-textRect.topLeft()))
        doc.documentLayout().draw(painter, ctx)

        painter.restore()
    
    def createEditor(self, parent, option, index):
        editor = QtWidgets.QTextEdit(parent)
        editor.setReadOnly(True)
        editor.setWordWrapMode(QtGui.QTextOption.NoWrap)
        editor.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        editor.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        return editor
    
    def setEditorData(self, editor, index):
        editor.setText(index.data())
    
    def setModelData(self, editor, model, index):
        pass
    
    def updateEditorGeometry (self, editor, option, index):
        editor.setGeometry(option.rect.adjusted(-2, -2, 2, 2)) #adjust for the etched borders of the QTextEdit
        
    def sizeHint(self, option, index):
        options = QtWidgets.QStyleOptionViewItem(option)
        self.initStyleOption(options,index)

        doc = QtGui.QTextDocument()
        doc.setHtml(options.text)
        return QtCore.QSize(int(doc.idealWidth()), int(doc.size().height()))
