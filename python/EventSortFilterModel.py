from PyQt5 import QtCore, QtGui, QtWidgets

from collections import namedtuple

def enum(**enums):
    return type('Enum', (), enums)
    
Filter = namedtuple('Filter', 'old_text new_text old_mask new_mask')
FilterType = enum(Source=1, Header=2, Status=4, Data=8)

class EventSortFilterModel(QtCore.QSortFilterProxyModel):

    def __init__(self, ui):
        QtCore.QSortFilterProxyModel.__init__(self)
        self.ui = ui
        self.setSourceModel(QtGui.QStandardItemModel())
        mask = ((self.ui.sourceButton.isChecked() and FilterType.Source or 0)
                 | (self.ui.headerButton.isChecked() and FilterType.Header or 0)
                 | (self.ui.statusButton.isChecked() and FilterType.Status or 0)
                 | (self.ui.dataButton.isChecked() and FilterType.Data or 0))
        self.filter = Filter('', '', mask, mask)
        self.showOnlyErrors = False
        
        ui.filterLineEdit.textChanged.connect(self.filterTextChanged)
        ui.sourceButton.toggled.connect(self.filterTypeChanged)
        ui.headerButton.toggled.connect(self.filterTypeChanged)
        ui.statusButton.toggled.connect(self.filterTypeChanged)
        ui.dataButton.toggled.connect(self.filterTypeChanged)
        ui.onlyErrorsButton.toggled.connect(self.showErrorsStateChanged)
        
    def appendRow(self, item):
        self.sourceModel().appendRow(item)
        
    def indexFromItem(self, item):
        return self.mapFromSource(self.sourceModel().indexFromItem(item))
        
    def setFilter(self, text, mask):
        self.filter = Filter(str(text), self.filter.new_text, mask, self.filter.new_mask)
        self.ui.filterLineEdit.setText(text)
        if mask & FilterType.Source:
            self.ui.sourceButton.setChecked(True)
        if mask & FilterType.Header:
            self.ui.headerButton.setChecked(True)
        if mask & FilterType.Data:
            self.ui.dataButton.setChecked(True)
        if mask & FilterType.Status:
            self.ui.statusButton.setChecked(True)
        self.applyFilter()
        
    def applyFilter(self):
        QtWidgets.QApplication.setOverrideCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))
        
        self.ui.eventsTreeView.setUpdatesEnabled(False)
        root = self.sourceModel().invisibleRootItem()
        for r in range(0, root.rowCount()):
            root.child(r).updateVisibility(self.filter)
        
        self.invalidate()
        self.ui.eventsTreeView.setUpdatesEnabled(True)
        QtWidgets.QApplication.restoreOverrideCursor()
    
    def showErrorsStateChanged(self, state):
        self.showOnlyErrors = state
        self.invalidate()   
    
    def filterTextChanged(self, text):
        self.filter = Filter(self.filter.new_text, str(text), self.filter.new_mask, self.filter.new_mask)
        self.applyFilter()
        
    def filterTypeChanged(self, toggled):
        mask = ((self.ui.sourceButton.isChecked() and FilterType.Source or 0)
                 | (self.ui.headerButton.isChecked() and FilterType.Header or 0)
                 | (self.ui.statusButton.isChecked() and FilterType.Status or 0)
                 | (self.ui.dataButton.isChecked() and FilterType.Data or 0))
        self.filter = Filter(self.filter.new_text, self.filter.new_text, self.filter.new_mask, mask)
        self.applyFilter()
        
    def filterAcceptsRow(self, row, parent_index):
        index = self.sourceModel().index(row, 0, parent_index)
        item = self.sourceModel().itemFromIndex(index)
        return item.isVisible(self.showOnlyErrors)

    def filterAcceptsColumn(self, column, parent_index):
        return True
