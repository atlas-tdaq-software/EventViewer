import os
import sys

from PyQt5 import QtCore, QtGui, QtWidgets

from .EventSortFilterModel import EventSortFilterModel
from .HTMLDelegate import HTMLDelegate
from .TreeFileItem import TreeFileItem
from .TreeEventItem import TreeEventItem
from .EventViewer_ui import Ui_EventViewer
from .AboutDialog_ui import Ui_AboutDialog
from . import EventViewer_rc

import ipc
import eformat
import emon
import ers

class About(QtWidgets.QDialog):
    def __init__(self):
        QtWidgets.QDialog.__init__(self)
        self.ui = Ui_AboutDialog()
        self.ui.setupUi(self)

class EventViewer(QtWidgets.QMainWindow):
    def __init__(self):
        self.app = QtWidgets.QApplication(sys.argv)
        QtWidgets.QMainWindow.__init__(self)

        # Set up the user interface from Designer.        
        self.ui = Ui_EventViewer()
        self.ui.setupUi(self)
        self.ui.eventsTreeView.sortByColumn(0, QtCore.Qt.AscendingOrder)
        self.ui.eventsTreeView.setItemDelegate(HTMLDelegate(self.ui.eventsTreeView))        
        self.ui.eventsTreeView.setModel(EventSortFilterModel(self.ui))
        
        self.ui.samplersTreeWidget.sortByColumn(0, QtCore.Qt.AscendingOrder)
        self.tabifyDockWidget(self.ui.sampleEventDockWidget, self.ui.openFileDockWidget)
        self.ui.sampleEventDockWidget.raise_()
        self.ui.selectionCriteriaTableWidget.resize(0, 0)
        
        # Connect up the widgets.
        self.ui.eventsTreeView.clicked.connect(self.ui.eventsTreeView.expand)
        
        self.ui.aboutButton_1.clicked.connect(self.showAbout)
        self.ui.aboutButton_2.clicked.connect(self.showAbout)
        
        self.ui.getEventButton.clicked.connect(self.getEvent)
        self.ui.refreshButton.clicked.connect(self.buildSamplersTree)
        self.ui.openFileButton.clicked.connect(self.openEventFile)
        
        self.ui.samplersTreeWidget.itemSelectionChanged.connect(self.samplersTreeSelectionChanged)
        self.ui.samplersTreeWidget.clicked.connect(self.ui.samplersTreeWidget.expand)
        
        self.ui.filesTreeWidget.itemDoubleClicked.connect(self.filesTreeDoubleClicked)
        self.ui.filesTreeWidget.clicked.connect(self.ui.filesTreeWidget.expand)
       
        self.buildSamplersTree()
        self.about = About()
        
        self.readSettings()

    def showAbout(self):
        self.about.show()
        
    def openEventFile(self):
        name, _filter_ = QtWidgets.QFileDialog.getOpenFileName(self, 'Open ATLAS data file', '', 'data (*.data *.raw);;all files (*)')
        if not name:
            return
        try:
            item = TreeFileItem(self.ui.filesTreeWidget, name)
        except Exception as e:
            QtWidgets.QApplication.restoreOverrideCursor()
            QtWidgets.QMessageBox.warning(self, "File reading error", repr(e))            
           
    def samplersTreeSelectionChanged(self):
        items = self.ui.samplersTreeWidget.selectedItems()
                
        if len(items) == 0:
            self.ui.getEventButton.setEnabled(False)
            return

        parent = items[0].parent()
        for item in items:
            if item.childCount() != 0 or not item.parent() == parent:
                self.ui.getEventButton.setEnabled(False) 
                return
            
        self.ui.getEventButton.setEnabled(True) 
        
    def filesTreeDoubleClicked(self, item, column):
        if item.parent() != None:
            event = item.parent().getEvent(item)
            self.showAtlasEvent(event)
        
    def buildSamplersTree(self):
        self.ui.samplersTreeWidget.clear()
        
        for partition in ipc.getPartitions():
            samplers = []
            try:
                samplers = emon.getEventSamplers(partition)
            except ers.Issue as e:
                continue
                
            if len(samplers) == 0:
                continue
            p_item = QtWidgets.QTreeWidgetItem(self.ui.samplersTreeWidget)
            p_item.setText(0, partition.name());
            p_item.setFlags(QtCore.Qt.ItemIsEnabled);
            types = {}
            for sampler in samplers:
                pair = sampler.split(':', 1)
                if pair[0] in types:
                    t = types[pair[0]]
                else:
                    t = QtWidgets.QTreeWidgetItem(p_item)
                    t.setText(0, pair[0])
                    t.setFlags(QtCore.Qt.ItemIsEnabled);
                    types[pair[0]] = t
                s = QtWidgets.QTreeWidgetItem(t)
                s.setText(0, pair[1])
                    
            
    def getEvent(self):
        items = self.ui.samplersTreeWidget.selectedItems()
        if len(items) == 0:
            return
        
        partitin_name = str(items[0].parent().parent().text(0))
        sampler_type = str(items[0].parent().text(0))
        sampler_names = len(items) == 1 and str(items[0].text(0)) or [str(i.text(0)) for i in items]
           
        partition = ipc.IPCPartition(partitin_name)
        address = emon.SamplingAddress(sampler_type, sampler_names)
        
        w = self.ui.selectionCriteriaTableWidget
        n = int(str(w.item(0,0).text())) if w.item(0,0).isSelected() else 0
        l1type = emon.L1TriggerType(n, not w.item(0,0).isSelected())
        
        l1 = emon.Logic.OR if w.item(1,0).isSelected() else emon.Logic.IGNORE
        bits = [int(b) for b in str(w.item(1,0).text()).split()]
        l1bits = emon.L1TriggerBits(bits, l1, emon.Origin.AFTER_VETO)
        
        l2 = emon.Logic.OR if w.item(2,0).isSelected() else emon.Logic.IGNORE
        streams = str(w.item(2,0).text()).split()
        stags = emon.StreamTags(streams[0] if len(streams) > 0 else '', 
                                streams[1:] if len(streams) > 0 else [], l2)
        
        n = int(str(w.item(3,0).text())) if w.item(3,0).isSelected() else 0
        sword = emon.StatusWord(n, not w.item(3,0).isSelected())
        criteria = emon.SelectionCriteria(l1type, l1bits, stags, sword)
        
        QtWidgets.QApplication.setOverrideCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))
        try:
            with emon.EventIterator(partition, address, criteria) as iterator:
                event = iterator.nextEvent(self.ui.timeoutSpinBox.value() * 1000)
                self.showAtlasEvent(eformat.FullEventFragment(event))
                QtWidgets.QApplication.restoreOverrideCursor()
        except ers.Issue as e:
            QtWidgets.QApplication.restoreOverrideCursor()
            if e.isInstanceOf('NoMoreEvents'):
                QtWidgets.QMessageBox.warning(self, "Event sampling error", 
                        """Got Timeout waiting for an event. """
                        """If DAQ rate is too low you can try to select multiple samplers of the same type"""
                        """ and/or increase the timeout""")
            else:
                QtWidgets.QMessageBox.warning(self, "Event sampling error", repr(e))
        except Exception as e:
            QtWidgets.QApplication.restoreOverrideCursor()
            QtWidgets.QMessageBox.warning(self, "Event sampling error",
                "Unexpected error of type " + repr(e))

    def showAtlasEvent(self, event):
        QtWidgets.QApplication.setOverrideCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))
        TreeEventItem(self.ui.eventsTreeView, event)
        QtWidgets.QApplication.restoreOverrideCursor()

    def closeEvent(self, event):
        settings = QtCore.QSettings("CERN", "EventViewer")
        settings.beginGroup("MainWindow");
        settings.setValue("size", self.size());
        settings.setValue("pos", self.pos());
        settings.setValue("filter_text", self.ui.eventsTreeView.model().filter.new_text);
        settings.setValue("filter_mask", self.ui.eventsTreeView.model().filter.new_mask);
        settings.endGroup();
        
    def readSettings(self):
        settings = QtCore.QSettings("CERN", "EventViewer")
        settings.beginGroup("MainWindow");
        self.resize(settings.value("size", QtCore.QSize(800, 600), type=QtCore.QSize));
        self.move(settings.value("pos", QtCore.QPoint(200, 200), type=QtCore.QPoint));
        self.ui.eventsTreeView.model().setFilter(
                settings.value("filter_text",'', type=str),
                settings.value("filter_mask",1, type=int))
        settings.endGroup();
        
    def run(self):
        self.show()    
        sys.exit(self.app.exec_())
            
