import re
from collections import namedtuple

class Context(object):
    def getCurrentROB(self):
            return self.rob
        
    def setCurrentROB(self, rob):
            self.rob = rob
        
    def getCurrentEvent(self):
            return self.event
        
    def setCurrentEvent(self, event):
            self.event = event
        
__plugin__ = namedtuple('plugin', 'pattern, regex, function')
__plugins__ = {}
__context__ = Context()

class Plugin(object):
    "Registers plugins"
        
    def __init__(self, type, source):
        self.type = type
        self.source = source
        
    def __call__(self, function):
        new = __plugin__(self.source, re.compile(self.source), function)
        if not self.type in __plugins__:
            __plugins__[self.type] = []
        else:
            for i,p in enumerate(__plugins__[self.type]):
                if p.pattern == new.pattern:
                    __plugins__[self.type][i] = new
                    return function
        __plugins__[self.type].insert(0, new)
        __plugins__[self.type].sort(key=lambda x: len(x.regex.pattern), reverse=True)
        return function

    @staticmethod
    def getContext():
        return __context__
        
    @staticmethod
    def translate(type, source, data):
        if not type in __plugins__:
            return data
            
        for p in __plugins__[type]:
            if p.regex.match(source):
                return p.function(data)
        return data
