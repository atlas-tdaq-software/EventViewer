from array import array
import datetime
from six import string_types

from PyQt5 import QtCore, QtGui, QtWidgets

from webemon import eformat2xml
from .EventSortFilterModel import FilterType

from .Plugin import Plugin

@Plugin("data", ".*")
@Plugin("rob_status", ".*")
@Plugin("rod_status", ".*")
@Plugin("l1_info", ".*")
@Plugin("l2_info", ".*")
@Plugin("ef_info", ".*")
@Plugin("event_status", ".*")
def default_converter(data):
    lines = [data[i:i+10] for i in range(0,len(data),10)]
    
    out = '<br />'.join(
                '<b><i>%04d</i></b> : %s' % (10*n,' '.join('0x%08x' % word for word in line))
                        for n,line in enumerate(lines))

    return '<font face="courier">' + out + '</font>'

class TreeNodeItem(QtGui.QStandardItem):
    def __init__(self):
        QtGui.QStandardItem.__init__(self)
        self.visibility = True
        self.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
        self.error = False
        self.selection = ''

    def isVisible(self, show_only_errors):
        if not show_only_errors:
            return self.visibility
        else:
            if self.error:
                return self.visibility
                
            if isinstance(self, RootNodeItem):
            	return self.visibility
            	
            p = self.parent()
            while not isinstance(p, RootNodeItem):
                if p.error:
                    return p.visibility
                p = p.parent()
            return False
        
    def setError(self):
        if self.error:
            return
            
        self.error = True
        self.setIcon(QtGui.QIcon(QtGui.QPixmap(":/images/error.png")))
        p = self.parent()
        if isinstance(p, TreeNodeItem):
            p.setError()
    
    def appendRow(self, item):
        QtGui.QStandardItem.appendRow(self, item)
        if item.error:
            self.setError()
        
    def clearSelection(self, text):
        if self.selection:
            t = self.text()
            self.setText(t.replace(self.selection, text))
            self.selection = ''
                       
    def updateVisibility(self, filter, parent_visible):
        self.clearSelection(filter.old_text)
        self.visibility = parent_visible or not filter.new_text
                
        if filter.new_text:
            t = self.text()
            text_found = filter.new_text in t
            self.visibility = self.visibility or text_found
            if text_found:
                self.selection = '<span style="background-color:yellow">' + filter.new_text + '</span>'
                t.replace(filter.new_text, self.selection)
                self.setText(t.replace(filter.new_text, self.selection))
        
        return self.visibility

class RootNodeItem(TreeNodeItem):
    def __init__(self, tag, source):
        TreeNodeItem.__init__(self)
        self.setText('<span style="color:purple; font-weight:bold;">%s</span> '
                     '<span style="color:blue">%s module=0x%04x</span>'
                        %(tag, source.human_detector(), source.module_id()))

    def updateVisibility(self, filter):
        self.visibility = True
        worker = lambda x: self.child(x).updateVisibility(filter, True)
        for x in range(0, self.rowCount()):
            worker(x)
        
        return self.visibility
        
class SourceNodeItem(TreeNodeItem):
    def __init__(self, source):
        TreeNodeItem.__init__(self)
        self.setText("""<span style="color:purple; font-weight:bold;">rob</span>
                        <span style="color:blue">%s module=0x%04x</span>
                        <span style="vertical-align:super"> (0x%x)</span>"""
                            %(source.human_detector(), source.module_id(), source.code()))

    def updateVisibility(self, filter, parent_visible):
        if filter.new_mask & FilterType.Source:
            TreeNodeItem.updateVisibility(self, filter, parent_visible)
        else:
            if filter.old_mask & FilterType.Source:
                self.clearSelection(filter.old_text)
            else:
                self.visibility = not filter.new_text
        for r in range(0, self.rowCount()):
            self.visibility = self.child(r).updateVisibility(filter, self.visibility) or self.visibility

        return self.visibility

class AttrNodeItem(TreeNodeItem):
    def __init__(self, name, value, show_as_hex=True, error=False):
        TreeNodeItem.__init__(self)
        vformat = isinstance(value, string_types) and '%s' or show_as_hex and '0x%x' or '%d'
        if error:
            format = '<small style="color:red; font-weight:bold">%s = ' + vformat + '</small>' 
        else:
            format = '<small>%s = <span style="color:blue">' + vformat + '</span>'
        
        self.setText(format %(name, value))
        
    def updateVisibility(self, filter, parent_visible):
        if filter.new_mask & FilterType.Header:
            TreeNodeItem.updateVisibility(self, filter, parent_visible)
        else:
            if filter.old_mask & FilterType.Header:
                self.clearSelection(filter.old_text)
            else:
                self.visibility = self.parent().visibility
        return self.visibility

class ValueNodeItem(TreeNodeItem):
    def __init__(self, value):
        TreeNodeItem.__init__(self)
        self.setText(value)
        self.setFlags(QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled)

class UserDataNodeItem(TreeNodeItem):
    def __init__(self, tag, value=None, error=False):
        TreeNodeItem.__init__(self)
        self.setText(tag)
        if value != None:
            item = ValueNodeItem(value)
            self.appendRow(item)
        if error:
            self.setError();
            
class DataNodeItemBase(TreeNodeItem):
    def __init__(self, tag, source, data, filter_type):
        TreeNodeItem.__init__(self)
        self.setText('<span style="color:black;font-weight:bold;">%s</span>'%tag)
        self.filter_type = filter_type
        result = Plugin.translate(tag, source.human_detector(), data)
        if isinstance(result, list):
            for tag,value,error in result:
                item = UserDataNodeItem(tag, value, error)
                self.appendRow(item)
                if item.error:
                    self.setError();
        else:
            if isinstance(result, tuple):
                (text, error) = result
                if error:
                    self.setError();
            else:
                text = result
            item = ValueNodeItem(text)
            self.appendRow(item)

    def updateVisibility(self, filter, parent_visible):
        if self.rowCount() != 0:
            if filter.new_mask & self.filter_type:
                self.visibility = self.child(0).updateVisibility(filter, False)
            else:
                if filter.old_mask & self.filter_type:
                    self.child(0).clearSelection(filter.old_text)
                self.visibility = parent_visible
        return self.visibility

class DataNodeItem(DataNodeItemBase):
    def __init__(self, tag, source, data):
        DataNodeItemBase.__init__(self, tag, source, data, FilterType.Data)

class StatusNodeItem(DataNodeItemBase):
    def __init__(self, tag, source, data):
        DataNodeItemBase.__init__(self, tag, source, data, FilterType.Status)

class TreeEventItem(RootNodeItem):
    def __init__(self, tree, event):
        RootNodeItem.__init__(self, 'full event', event.source_id())
        self.event = event
        self.tree = tree
        
        self.parseEvent(event)
        self.updateVisibility(tree.model().filter)
        self.tree.model().appendRow(self)
        
        self.frame = QtWidgets.QFrame()
        self.layout = QtWidgets.QHBoxLayout(self.frame)
        self.layout.setContentsMargins(0, 0, 10, 0)
        self.layout.addItem(QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))

        self.save = QtWidgets.QToolButton(self.frame)
        self.save.setIcon(QtGui.QIcon(QtGui.QPixmap(":/images/save_as.png")))
        self.save.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed))
        self.layout.addWidget(self.save)
        
        self.remove = QtWidgets.QToolButton(self.frame)
        self.remove.setIcon(QtGui.QIcon(QtGui.QPixmap(":/images/remove.png")))
        self.remove.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed))
        self.layout.addWidget(self.remove)        
        self.tree.setIndexWidget(self.tree.model().indexFromItem(self), self.frame)
        
        self.remove.clicked.connect(self.removeClicked)
        self.save.clicked.connect(self.saveClicked)
            
    def removeClicked(self):
        self.tree.model().removeRow(self.tree.model().indexFromItem(self).row())
        
    def saveClicked(self):
        (name, filter) = map(str,
                             QtWidgets.QFileDialog.getSaveFileName(
                             self.tree, 'Save as XML file', '', 'XML (*.xml);; RAW (*.raw)'))
                
        if len(name) == 0:
            return
        
        try:
            if filter == 'RAW (*.raw)':
                if not name.endswith('.raw'):
                    name += '.raw'
                file = open(name,'wb')
                array('I', self.event.__raw__()).tofile(file)
                file.close()
            else:
                if not name.endswith('.xml'):
                    name += '.xml'
                file = open(name,'w')
                file.write(eformat2xml.eventToXML(self.event))
                file.close()
        except Exception as e:
            QtWidgets.QMessageBox.warning(self.tree, str(e), repr(e))

    def checkAttribute(self, node, name, value, target, show_as_hex=True):
        if value != target:
            item = AttrNodeItem(name, value, show_as_hex, True)
            node.appendRow(item)
            node.setError()

    def parseEvent(self, event):                            
        Plugin.getContext().setCurrentEvent(event)
        
        run_number = event.run_no()
        bc_id = event.bc_id()
        l1_id = event.lvl1_id()
        l1_trigger_type = event.lvl1_trigger_type()
        
        self.appendRow(AttrNodeItem('run_type', str(event.run_type())))
        self.appendRow(AttrNodeItem('run_number', event.run_no(), False))
        self.appendRow(AttrNodeItem('lumi_block', event.lumi_block(), False))
        self.appendRow(AttrNodeItem('global_id', event.global_id()))
        self.appendRow(AttrNodeItem('bcid', event.bc_id()))
        self.appendRow(AttrNodeItem('bc_time', 
                datetime.datetime.fromtimestamp(event.bc_time_seconds()).strftime('%Y-%m-%d %H:%M:%S') 
                + "." + 
                str(event.bc_time_nanoseconds()).zfill(9)))
        self.appendRow(AttrNodeItem('l1_id', event.lvl1_id()))
        self.appendRow(AttrNodeItem('l1_trigger_type', event.lvl1_trigger_type()))
        self.appendRow(AttrNodeItem('version', str(event.version())))
        
        if event.checksum_value() != 0:
            self.appendRow(AttrNodeItem('checksum', event.checksum_value()))

        for cnt, s in enumerate(event.stream_tag()):
            self.appendRow(AttrNodeItem(
                'stream%d' % cnt, ', '.join([s.name, s.type, str(s.obeys_lumiblock)])))

        if len(event.status()) > 0:
            self.appendRow(StatusNodeItem('event_status', event.source_id(), event.status()))
        if len(event.event_filter_info()) > 0:
            self.appendRow(StatusNodeItem('ef_info', event.source_id(), event.event_filter_info()))
        if len(event.lvl1_trigger_info()) > 0:
            self.appendRow(StatusNodeItem('l1_info', event.source_id(), event.lvl1_trigger_info()))
        if len(event.lvl2_trigger_info()) > 0:
            self.appendRow(StatusNodeItem('l2_info', event.source_id(), event.lvl2_trigger_info()))
        
        it = event.children()
        cnt = 0
        while True:
            try:
                rob = it.next()
                Plugin.getContext().setCurrentROB(rob)
                
                rob_node = SourceNodeItem(rob.rob_source_id())
                self.appendRow(rob_node)
                
                self.checkAttribute(rob_node, "run_number", rob.rod_run_no(), run_number, False)
                self.checkAttribute(rob_node, "l1_id", rob.rod_lvl1_id(), l1_id)
                self.checkAttribute(rob_node, "bc_id", rob.rod_bc_id(), bc_id)
                self.checkAttribute(rob_node, "l1_trigger_type", rob.rod_lvl1_trigger_type(), l1_trigger_type)
                self.checkAttribute(rob_node, "rod_source_id", rob.rod_source_id().code(), rob.rob_source_id().code())
                                        
                if rob.checksum_value() != 0:
                    rob_node.appendRow(AttrNodeItem("checksum", rob.checksum_value()))
                    
                rob_node.appendRow(AttrNodeItem("rob_version", str(rob.version())))
                rob_node.appendRow(AttrNodeItem("det_event_type", rob.rod_detev_type()))
                rob_node.appendRow(AttrNodeItem("rod_version", str(rob.rod_version())))
                
                if len(rob.status()) > 0:
                    rob_node.appendRow(StatusNodeItem('rob_status', rob.rob_source_id(), rob.status()))
                if len(rob.rod_status()) > 0:
                    rob_node.appendRow(StatusNodeItem('rod_status', rob.rod_source_id(), rob.rod_status()))
                rob_node.appendRow(DataNodeItem('data', rob.rod_source_id(), rob.rod_data()))
                
            except RuntimeError as e:
                print(e)
            except StopIteration:
                break
