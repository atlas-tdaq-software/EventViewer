import datetime

from PyQt5 import QtCore, QtGui, QtWidgets

from eformat import istream

class TreeFileItem(QtWidgets.QTreeWidgetItem):
    def __init__(self, tree, file_name):
        QtWidgets.QTreeWidgetItem.__init__(self)
        self.tree = tree
        self.file_name = file_name
        self.setText(0, self.file_name)
        self.estream = istream(str(self.file_name))
        for (index, event) in enumerate(self.estream):
            item = QtWidgets.QTreeWidgetItem(self)
            item.setData(0, QtCore.Qt.UserRole, index)
            id = str(event.global_id()) + ' (' + datetime.datetime.fromtimestamp(
                    event.bc_time_seconds()).strftime('%Y-%m-%d %H:%M:%S') + ')'
            item.setText(0, id)
        
        self.frame = QtWidgets.QFrame()
        self.layout = QtWidgets.QHBoxLayout(self.frame)
        self.layout.setContentsMargins(0, 0, 0, 0)

        self.remove = QtWidgets.QToolButton(self.frame)
        self.remove.setIcon(QtGui.QIcon(QtGui.QPixmap(":/images/remove.png")))
        self.remove.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed))
        self.layout.addWidget(self.remove)
        self.layout.addItem(QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))

        self.tree.addTopLevelItem(self)
        self.tree.setItemWidget(self, 0, self.frame)
        self.remove.clicked.connect(self.removeClicked)
            
    def setText(self, column, string):
        QtWidgets.QTreeWidgetItem.setText(self, column, '      ' + string)

    def getEvent(self, item):
        index = item.data(0, QtCore.Qt.UserRole)
        return self.estream[index]
        
    def removeClicked(self):
        self.tree.takeTopLevelItem(self.tree.indexOfTopLevelItem(self))
        
