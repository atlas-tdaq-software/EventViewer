import EventViewer

@EventViewer.Plugin("data", "LAR_.*")
@EventViewer.Plugin("rod_status", "LAR_.*")
def lar_converter(data):
    lines = [data[i:i+10] for i in range(0,len(data),10)]

#uncomment this code if you need Event or ROB header values
#    current_rob = EventViewer.Plugin.getContext().getCurrentROB()         # returns libpyeformat.ROBFragment
#    current_event = EventViewer.Plugin.getContext().getCurrentEvent()     # returns libpyeformat.FullEventFragment
    
    out = '<br />'.join(
                '<b><i>%03d</i></b> : %s' % (10*n,' '.join('0x%08x' % word for word in line))
                        for n,line in enumerate(lines))

    return '<font face="courier" color="blue">' + out + '</font>'

@EventViewer.Plugin("data", "LAR_EM_.*")
def lar_em_data_converter(data):
    lines = [data[i:i+10] for i in range(0,len(data),10)]

#uncomment this code if you need Event or ROB header values
#    current_rob = EventViewer.Plugin.getContext().getCurrentROB()         # returns libpyeformat.ROBFragment
#    current_event = EventViewer.Plugin.getContext().getCurrentEvent()     # returns libpyeformat.FullEventFragment

    out = '<br />'.join(
                '<b><i>%03d</i></b> : %s' % (10*n,' '.join('0x%08x' % word for word in line))
                        for n,line in enumerate(lines))

    return '<font face="courier" color="red">' + out + '</font>'

@EventViewer.Plugin("rod_status", "LAR_EM_.*")
def lar_em_status_converter(data):
    lines = [data[i:i+10] for i in range(0,len(data),10)]

#uncomment this code if you need Event or ROB header values
#    current_rob = EventViewer.Plugin.getContext().getCurrentROB()         # returns libpyeformat.ROBFragment
#    current_event = EventViewer.Plugin.getContext().getCurrentEvent()     # returns libpyeformat.FullEventFragment

    out = '<br />'.join(
                '<b><i>%03d</i></b> : %s' % (10*n,' '.join('0x%08x' % word for word in line))
                        for n,line in enumerate(lines))

    return ('<font face="courier" color="green">' + out + '</font>', True)
