#!/usr/bin/env tdaq_python

import sys
from EventViewer import EventViewer

if __name__ == '__main__':
    for a in sys.argv[1:]:
        __import__(a, globals(), locals(), [], 0)
    
    w = EventViewer.EventViewer()
    w.run()
